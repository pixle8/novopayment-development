module.exports = function(grunt) {
    grunt.initConfig({
        sass: { // Task
            dist: {  // Target
                options: { // Target options
                    style: 'compressed'
                },

                files: { // Dictionary of files
                    'css/style.css': 'css/style.scss', // 'destination': 'source'
                }
            }
        },

        postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')({browsers: ['last 3 versions']})
                ]
            },
            dist: {
                src: 'css/style.css'
            }
        },

        watch: {
            styles: {
                files: '**/*.scss',
                tasks: ['sass'],
                options: {
                    debounceDelay: 250,
                },
            },
        },

        imagemin: { // Task
            dynamic: { // Another target
                options: { // Target options
                    optimizationLevel: 3
                },

                files: [{
                    expand: true,                  // Enable dynamic expansion
                    cwd: 'images/original/',                   // Src matches are relative to this path
                    src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
                    dest: 'images/compressed/'                  // Destination path prefix
                }]
            }
        }
    });

    // grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    grunt.registerTask('default', ['sass', 'postcss', 'watch']);
};
