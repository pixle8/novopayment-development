<?php include("incl/header.php"); ?>
	<section class="header" id="home-header">
		<div class="container">
			<div class="header-bar">
				<a href="home.php" class="logo-wrap">
					<img src="images/logo.png" alt="" class="logo">
				</a>
				<div class="nav-menu-wrap">
					<ul>
						<li><a href="#">Solutions</a></li>
						<li><a href="#">Markets</a></li>
						<li><a href="#">About Us</a></li>
						<li class="active"><a href="#">Contact</a></li>
					</ul>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="banner-content">
				<div class="text-wrap">
					<h1>Enabling payments innovation throughout the Americas.</h1>
					<a href="#" class="button green solid">Get in touch</a>
				</div>
			</div>
		</div>
	</section>
	<section id="innovative">
		<div class="container">
			<div class="third innovative-third">
				<h2>Innovative <br>cost-effective solutions</h2>
				<p>If you’ve identified a new revenue opportunity, we’ll work with you to design, launch, and scale new services to capture it.</p>
			</div>
			<div class="third innovative-third">
				<h2>Accelerate deployment of new capabilities</h2>
				<p>Our experienced team of experts can help you introduce new payments programs to market in as little as 90 days. </p>
			</div>
			<div class="third innovative-third">
				<h2>Lower your transactional costs</h2>
				<p>With our solutions, you’ll be able to minimize the duplication of systems, processes, and long-term maintenance costs.</p>
			</div>
		</div>
	</section>
	<section id="values">
		<div class="container">
			<h3>Our value proposition</h3>
			<div id="values-boxes-wrap">
				<ul class="values-items">
					<li>
						<div class="values-box">
							<div class="image-wrap">
								<img src="images/clock.png" alt="">
							</div>
							<div class="text-wrap">
								<p>Accelerated Time-to-Market</p>
							</div>
						</div>
					</li>
					<li>
						<div class="values-box">
							<div class="image-wrap">
								<img src="images/earth.png" alt="">
							</div>
							<div class="text-wrap">
								<p>Regional Interoperability</p>
							</div>
						</div>
					</li>
					<li>
						<div class="values-box">
							<div class="image-wrap">
								<img src="images/circle-graph.png" alt="">
							</div>
							<div class="text-wrap">
								<p>Payment Optimization</p>
							</div>
						</div>
					</li>
					<li>
						<div class="values-box">
							<div class="image-wrap">
								<img src="images/lock-shield.png" alt="">
							</div>
							<div class="text-wrap">
								<p>Bank-Grade Security</p>
							</div>
						</div>
					</li>
					<li>
						<div class="values-box">
							<div class="image-wrap">
								<img src="images/bubble-trio.png" alt="">
							</div>
							<div class="text-wrap">
								<p>Interconnent Multiple Ecosystems</p>
							</div>
						</div>
					</li>
					<li>
						<div class="values-box">
							<div class="image-wrap">
								<img src="images/cloud.png" alt="">
							</div>
							<div class="text-wrap">
								<p>Virtual Stored-Value Accounts</p>
							</div>
						</div>
					</li>
					<li>
						<div class="values-box">
							<div class="image-wrap">
								<img src="images/chain-link.png" alt="">
							</div>
							<div class="text-wrap">
								<p>Ease of Integration</p>
							</div>
						</div>
					</li>
					<li>
						<div class="values-box">
							<div class="image-wrap">
								<img src="images/weird-recycle.png" alt="">
							</div>
							<div class="text-wrap">
								<p>End-to-End Processing</p>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</section>

<?php include("incl/footer.php"); ?>


























